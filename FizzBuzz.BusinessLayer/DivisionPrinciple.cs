﻿namespace FizzBuzz.BusinessLayer
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public abstract class DivisionPrinciple : IDivisionPrinciple
    {
        public int Order { get; set; }

        public DayOfWeek Day { get; set; }

        protected int DivisionBy { get; set; }

        protected string Text { get; set; }

        protected string WednesdayText { get; set; }


        public string DivisionMethod(int number)
        {
            if (number % this.DivisionBy != 0)
            {
                return number.ToString(CultureInfo.CurrentCulture);
            }
            return (this.Day != DayOfWeek.Wednesday) ? this.Text : this.WednesdayText;

        }
    }
}
