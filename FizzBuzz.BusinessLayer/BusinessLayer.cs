﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace FizzBuzz.BusinessLayer
{
    public class BusinessLayer : IBusinessLayer
    {
        private readonly IEnumerable<IDivisionPrinciple> listDivisionsRules;

        public BusinessLayer(IEnumerable<IDivisionPrinciple> listDivisibleRules)
        {
            this.listDivisionsRules = listDivisibleRules;
        }

        public IEnumerable<string> GenerateFizzBuzzSequence(int inputNumber)
        {

            var fizzbuzzCollection = new List<string>();
            for (int counter = 1; counter <= inputNumber; counter++)
            {
                string divisibleOutput = null;
                var sortedDivisionRules = this.listDivisionsRules.OrderBy(m => m.Order);
                foreach (var divisibleRule in sortedDivisionRules)
                {
                    divisibleOutput = divisibleRule.DivisionMethod(counter);
                    if (divisibleOutput != counter.ToString(CultureInfo.CurrentCulture))
                    {
                        break;
                    }
                }

                fizzbuzzCollection.Add(divisibleOutput);
            }

            return fizzbuzzCollection;
        }
    }
}
