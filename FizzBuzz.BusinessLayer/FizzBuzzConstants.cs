﻿namespace FizzBuzz.BusinessLayer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class FizzBuzzConstants
    {
        public const string Fizz = "fizz";
        public const string Wizz = "wizz";
        public const string Buzz = "buzz";
        public const string Wuzz = "wuzz";
        public const string FizzBuzz = "fizz buzz";
        public const string WizzWuzz = "wizz wuzz";
    }
}
