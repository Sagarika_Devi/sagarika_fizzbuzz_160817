﻿namespace FizzBuzz.BusinessLayer
{
    using System;
    using System.Globalization;

    public class DivisionByThreeAndFivePrinciple : DivisionPrinciple
    {
        public DivisionByThreeAndFivePrinciple()
        {
            this.DivisionBy = 15;
            this.Text = FizzBuzzConstants.FizzBuzz;
            this.WednesdayText = FizzBuzzConstants.WizzWuzz;

        }
    }
}
