﻿namespace FizzBuzz.BusinessLayer
{
    using System;

    public interface IDivisionPrinciple
    {
        int Order { get; set; }
        DayOfWeek Day { get; }

        string DivisionMethod(int number);
    }
}
