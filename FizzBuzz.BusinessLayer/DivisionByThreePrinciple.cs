﻿namespace FizzBuzz.BusinessLayer
{
    using System;
    using System.Globalization;

    public class DivisionByThreePrinciple : DivisionPrinciple
    {
        public DivisionByThreePrinciple()
        {
            this.DivisionBy = 3;
            this.Text = FizzBuzzConstants.Fizz;
            this.WednesdayText = FizzBuzzConstants.Wizz;


        }
    }
}
