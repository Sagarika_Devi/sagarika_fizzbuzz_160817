﻿namespace FizzBuzz.BusinessLayer
{
    using System;
    using System.Globalization;

    public class DivisionByFivePrinciple : DivisionPrinciple
    {
        public DivisionByFivePrinciple()
        {
            this.DivisionBy = 5;
            this.Text = FizzBuzzConstants.Buzz;
            this.WednesdayText = FizzBuzzConstants.Wuzz;

        }
    }
}
