﻿namespace FizzBuzz.BusinessLayer
{
    using System;
    using System.Collections.Generic;

    public interface IBusinessLayer
    {
        IEnumerable<string> GenerateFizzBuzzSequence(int inputNumber);
    }
}
