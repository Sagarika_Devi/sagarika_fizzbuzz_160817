﻿using FizzBuzz.WebApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FizzBuzz.BusinessLayer;
using PagedList;

namespace FizzBuzz.WebApplication.Controllers
{
    public class FizzBuzzController : Controller
    {
        private const int PageSize = 20;

        private readonly IBusinessLayer businessLayer;

        public FizzBuzzController(IBusinessLayer ibusinessLayer)
        {
            this.businessLayer = ibusinessLayer;
        }
        // GET: FizzBuzz

        [HttpGet]
        public ActionResult FizzBuzzList()
        {
            var fizzBuzzModel = new FizzBuzzModel();
            return this.View(fizzBuzzModel);
        }
        [HttpPost]
        public ActionResult FizzBuzzList(FizzBuzzModel model)
        {
            if (ModelState.IsValid)
            {
                model = this.GetFizzBuzzSequence(model.Input.Value, 1);
                ModelState.Clear();
            }

            return this.View("FizzBuzzList", model);
        }
        [HttpGet]
        public ActionResult ResultPage(int enteredValue, int pageNumber)
        {
            var model = this.GetFizzBuzzSequence(enteredValue, pageNumber);
            return this.View("FizzBuzzList", model);
        }
        private FizzBuzzModel GetFizzBuzzSequence(int inputNumber, int pageNumber)
        {
            var fizzBuzzList = this.businessLayer.GenerateFizzBuzzSequence(inputNumber);
            var model = new FizzBuzzModel { Input = inputNumber, EnteredValue = inputNumber, FizzBuzzList = fizzBuzzList.ToPagedList(pageNumber, PageSize) };
            return model;
        }

    }
}
