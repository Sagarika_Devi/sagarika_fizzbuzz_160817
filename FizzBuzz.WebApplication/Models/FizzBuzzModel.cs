﻿using System.Web.UI.WebControls;

namespace FizzBuzz.WebApplication.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.Collections.Generic;
    using PagedList;

    public class FizzBuzzModel
    {
        public int EnteredValue { get; set; }

        [Required(ErrorMessage = @"*Please enter {0}")]
        [RegularExpression("^[1-9][0-9]*$", ErrorMessage = "Please enter a positive integer")]
        [Range(typeof(int), "1", "1000", ErrorMessage = @"Please enter {0} between  {1} and {2}")]
        public int? Input { get; set; }

        public IPagedList<string> FizzBuzzList { get; set; }


    }
}
