﻿namespace FizzBuzz.WebApplication
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using FizzBuzz.BusinessLayer;
    using Microsoft.Practices.Unity;
    using Unity.Mvc4;

    public static class FizzBuzzUnityContainer
    {
        public static IUnityContainer GlobalUnityContainer { get; set; }

        public static void Initialise()
        {
            GlobalUnityContainer = BuildUnityContainer();
            DependencyResolver.SetResolver(new UnityDependencyResolver(GlobalUnityContainer));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();
            var divisionRules = new List<IDivisionPrinciple>();
            divisionRules.Add(new DivisionByThreeAndFivePrinciple() { Order = 1, Day = DateTime.Now.DayOfWeek });
            divisionRules.Add(new DivisionByThreePrinciple() { Order = 2, Day = DateTime.Now.DayOfWeek });
            divisionRules.Add(new DivisionByFivePrinciple() { Order = 3, Day = DateTime.Now.DayOfWeek });

            container.RegisterInstance<IEnumerable<IDivisionPrinciple>>(divisionRules);
            container.RegisterType<IBusinessLayer, FizzBuzz.BusinessLayer.BusinessLayer>();
            return container;
        }
    }
}
