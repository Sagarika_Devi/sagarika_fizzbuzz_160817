﻿namespace FizzBuzz.BusinessLayer.Tests
{
    using System;
    using FluentAssertions;
    using NUnit.Framework;
    using FizzBuzz.BusinessLayer;

    [TestFixture]
    public class DivisionByThreePrincipleTest
    {
        private DivisionByThreePrinciple threeDivisionRule;

        [SetUp]
        public void Initialise()
        {
            this.threeDivisionRule = new DivisionByThreePrinciple();
        }
        [TestCase(DayOfWeek.Monday)]
        [TestCase(DayOfWeek.Tuesday)]
        [TestCase(DayOfWeek.Thursday)]
        [TestCase(DayOfWeek.Friday)]
        [TestCase(DayOfWeek.Saturday)]
        [TestCase(DayOfWeek.Sunday)]
        public void DivisionMethodReturnsFizzForDivisibleByThreeAndDayIsNonWednesDay(DayOfWeek day)
        {
            this.threeDivisionRule.Day = day;
            var result = this.threeDivisionRule.DivisionMethod(3);
            Assert.AreEqual(FizzBuzzConstants.Fizz, result);
        }


        [Test]
        public void DivisionMethodReturnsWizzForDivisibleByThreeAndDayIsWednesDay()
        {
            this.threeDivisionRule.Day = DayOfWeek.Wednesday;
            var result = this.threeDivisionRule.DivisionMethod(3);
            Assert.AreEqual(FizzBuzzConstants.Wizz, result);
        }

        [Test]
        public void DivisionMethodReturnsNumberForNotDivisibleByThree()
        {
            var result = this.threeDivisionRule.DivisionMethod(4);
            Assert.AreNotEqual(FizzBuzzConstants.Fizz, result);
            Assert.AreEqual("4", result);
        }
        [TearDown]
        public void Dispose()
        {
            this.threeDivisionRule = null;
        }
    }
}
