﻿namespace FizzBuzz.BusinessLayer.Tests
{
    using System;
    using FluentAssertions;
    using NUnit.Framework;
    using FizzBuzz.BusinessLayer;

    [TestFixture]
    public class DivisionByThreeAndFivePrincipleTest
    {
        private DivisionByThreeAndFivePrinciple threeAndFiveDivisionRule;

        [SetUp]
        public void Initialise()
        {
            this.threeAndFiveDivisionRule = new DivisionByThreeAndFivePrinciple();
        }
        [TestCase(DayOfWeek.Monday)]
        [TestCase(DayOfWeek.Tuesday)]
        [TestCase(DayOfWeek.Thursday)]
        [TestCase(DayOfWeek.Friday)]
        [TestCase(DayOfWeek.Saturday)]
        [TestCase(DayOfWeek.Sunday)]
        public void DivisionMethodReturnsFizzBuzzForDivisibleByThreeAndFiveAndDayIsNonWednesDay(DayOfWeek day)
        {
            this.threeAndFiveDivisionRule.Day = day;
            var result = this.threeAndFiveDivisionRule.DivisionMethod(15);
            Assert.AreEqual(FizzBuzzConstants.FizzBuzz, result);
        }


        [Test]
        public void DivisionMethodReturnsWizzWuzzForDivisibleByThreeAndFiveAndDayIsWednesDay()
        {
            this.threeAndFiveDivisionRule.Day = DayOfWeek.Wednesday;
            var result = this.threeAndFiveDivisionRule.DivisionMethod(15);
            Assert.AreEqual(FizzBuzzConstants.WizzWuzz, result);
        }

        [Test]
        public void DivisionMethodReturnsNumberForNotDivisibleByThreeAndFive()
        {
            var result = this.threeAndFiveDivisionRule.DivisionMethod(14);
            Assert.AreNotEqual(FizzBuzzConstants.FizzBuzz, result);
            Assert.AreEqual("14", result);
        }
        [TearDown]
        public void Dispose()
        {
            this.threeAndFiveDivisionRule = null;
        }
    }
}
