﻿using System;

namespace FizzBuzz.BusinessLayer.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using FizzBuzz.BusinessLayer;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class BusinessLayerTest
    {
        private BusinessLayer businessLayer;

        [SetUp]
        public void IntializeObjects()
        {
            var threefivedivisible = new Mock<IDivisionPrinciple>();
            threefivedivisible.Setup(m => m.Order).Returns(1);
            threefivedivisible.Setup(m => m.DivisionMethod(It.IsAny<int>())).Returns((int i) => (i % 15 == 0 ? "fizz buzz" : i.ToString()));

            var threedivisible = new Mock<IDivisionPrinciple>();
            threedivisible.Setup(m => m.Order).Returns(2);
            threedivisible.Setup(m => m.DivisionMethod(It.IsAny<int>())).Returns((int i) => (i % 3 == 0 ? "fizz" : i.ToString()));

            var fivedivisible = new Mock<IDivisionPrinciple>();
            fivedivisible.Setup(m => m.Order).Returns(3);
            fivedivisible.Setup(m => m.DivisionMethod(It.IsAny<int>())).Returns((int i) => (i % 5 == 0 ? "buzz" : i.ToString()));


            var divisibles = new List<IDivisionPrinciple>() { threefivedivisible.Object, threedivisible.Object, fivedivisible.Object };
            this.businessLayer = new BusinessLayer(divisibles);

        }

        [Test]
        public void FizzBuzzSequenceReturnsValidSequence()
        {
            var result = this.businessLayer.GenerateFizzBuzzSequence(15);
            result.Count().Should().Be(15);
            result.ElementAt(0).Should().Be("1");
            result.ElementAt(2).Should().Be("fizz");
            result.ElementAt(4).Should().Be("buzz");
            result.ElementAt(14).Should().Be("fizz buzz");

        }

        [Test]
        public void FizzBuzzSequenceOnWednesday()
        {
            var threefivedivisible = new Mock<IDivisionPrinciple>();
            threefivedivisible.Setup(m => m.Order).Returns(1);
            threefivedivisible.Setup(m => m.Day).Returns(DayOfWeek.Wednesday);
            threefivedivisible.Setup(m => m.DivisionMethod(It.IsAny<int>())).Returns((int i) => (i % 15 == 0 ? "wizz wuzz" : i.ToString()));

            var threedivisible = new Mock<IDivisionPrinciple>();
            threedivisible.Setup(m => m.Order).Returns(2);
            threedivisible.Setup(m => m.Day).Returns(DayOfWeek.Wednesday);
            threedivisible.Setup(m => m.DivisionMethod(It.IsAny<int>())).Returns((int i) => (i % 3 == 0 ? "wizz" : i.ToString()));

            var fivedivisible = new Mock<IDivisionPrinciple>();
            fivedivisible.Setup(m => m.Order).Returns(3);
            fivedivisible.Setup(m => m.Day).Returns(DayOfWeek.Wednesday);
            fivedivisible.Setup(m => m.DivisionMethod(It.IsAny<int>())).Returns((int i) => (i % 5 == 0 ? "wuzz" : i.ToString()));


            var divisibles = new List<IDivisionPrinciple>() { threefivedivisible.Object, threedivisible.Object, fivedivisible.Object };
            this.businessLayer = new BusinessLayer(divisibles);

            var result = this.businessLayer.GenerateFizzBuzzSequence(15);
            result.Count().Should().Be(15);
            result.ElementAt(0).Should().Be("1");
            result.ElementAt(2).Should().Be("wizz");
            result.ElementAt(4).Should().Be("wuzz");
            result.ElementAt(14).Should().Be("wizz wuzz");
        }
        [TearDown]
        public void Dispose()
        {
            this.businessLayer = null;
        }
    }
}
