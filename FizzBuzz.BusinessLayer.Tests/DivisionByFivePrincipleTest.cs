﻿namespace FizzBuzz.BusinessLayer.Tests
{
    using System;
    using FluentAssertions;
    using NUnit.Framework;
    using FizzBuzz.BusinessLayer;

    [TestFixture]
    public class DivisionByFivePrincipleTest
    {
        private DivisionByFivePrinciple fiveDivisionRule;

        [SetUp]
        public void Initialise()
        {
            this.fiveDivisionRule = new DivisionByFivePrinciple();
        }
        [TestCase(DayOfWeek.Monday)]
        [TestCase(DayOfWeek.Tuesday)]
        [TestCase(DayOfWeek.Thursday)]
        [TestCase(DayOfWeek.Friday)]
        [TestCase(DayOfWeek.Saturday)]
        [TestCase(DayOfWeek.Sunday)]
        public void DivisionMethodReturnsBuzzForDivisibleByFiveAndDayIsNonWednesDay(DayOfWeek day)
        {
            this.fiveDivisionRule.Day = day;
            var result = this.fiveDivisionRule.DivisionMethod(5);
            Assert.AreEqual(FizzBuzzConstants.Buzz, result);
        }


        [Test]
        public void DivisionMethodReturnsWuzzForDivisibleByFiveAndDayIsWednesDay()
        {
            this.fiveDivisionRule.Day = DayOfWeek.Wednesday;
            var result = this.fiveDivisionRule.DivisionMethod(5);
            Assert.AreEqual(FizzBuzzConstants.Wuzz, result);
        }

        [Test]
        public void DivisionMethodReturnsNumberForNotDivisibleByFive()
        {
            var result = this.fiveDivisionRule.DivisionMethod(4);
            Assert.AreNotEqual(FizzBuzzConstants.Buzz, result);
            Assert.AreEqual("4", result);
        }
        [TearDown]
        public void Dispose()
        {
            this.fiveDivisionRule = null;
        }
    }
}
