﻿using System.Linq;

namespace FizzBuzz.WebApplication.Tests
{
    using System.Collections.Generic;
    using FizzBuzz.WebApplication.Controllers;
    using FluentAssertions;
    using Models;
    using Moq;
    using System.Web.Mvc;
    using NUnit.Framework;
    using FizzBuzz.BusinessLayer;


    public class FizzBuzzControllerTests
    {
        private FizzBuzzController fizzbuzzcontroller;
        private Mock<IBusinessLayer> mockbunisessLayer;
        private FizzBuzzModel modelInstance;

        [SetUp]
        public void Initialize()
        {
            modelInstance = new FizzBuzzModel { Input = 15 };
            mockbunisessLayer = new Mock<IBusinessLayer> { CallBase = true };

            mockbunisessLayer.Setup(m => m.GenerateFizzBuzzSequence(15)).Returns(
                new List<string>
                {
                    "1",
                    "2",
                    "fizz",
                    "4",
                    "buzz",
                    "fizz",
                    "7",
                    "8",
                    "fizz",
                    "buzz",
                    "11",
                    "fizz",
                    "13",
                    "14",
                    "fizz buzz"

                });
            this.fizzbuzzcontroller = new FizzBuzzController(this.mockbunisessLayer.Object);
        }

        [Test]
        public void FizzBuzzReturnsValidList()
        {
            ViewResult actual = this.fizzbuzzcontroller.FizzBuzzList(this.modelInstance) as ViewResult;
            Assert.AreEqual(mockbunisessLayer.Object.GenerateFizzBuzzSequence(15).ToList(), ((FizzBuzzModel)actual.Model).FizzBuzzList.ToList());
            actual.Model.As<FizzBuzzModel>().FizzBuzzList.ElementAt(0).Should().Be("1");

            actual.Model.As<FizzBuzzModel>().FizzBuzzList.ElementAt(2).Should().Be("fizz");


            actual.Model.As<FizzBuzzModel>().FizzBuzzList.ElementAt(4).Should().Be("buzz");
            actual.Model.As<FizzBuzzModel>().FizzBuzzList.ElementAt(14).Should().Be("fizz buzz");

        }
        [Test]
        public void FizzBuzzListForWednesday()
        {
            this.mockbunisessLayer.Setup(m => m.GenerateFizzBuzzSequence(15)).Returns(
                new List<string>
                {
                    "1",
                    "2",
                    "wizz",
                    "4",
                    "wuzz",
                    "wizz",
                    "7",
                    "8",
                    "wizz",
                    "wuzz",
                    "11",
                    "wizz",
                    "13",
                    "14",
                    "wizz wuzz"
                });
            this.fizzbuzzcontroller = new FizzBuzzController(this.mockbunisessLayer.Object);
            var actual = this.fizzbuzzcontroller.FizzBuzzList(this.modelInstance) as ViewResult;

            ((FizzBuzzModel)actual.Model).FizzBuzzList.ElementAt(0).Should().Be("1");
            ((FizzBuzzModel)actual.Model).FizzBuzzList.ElementAt(2).Should().Be(FizzBuzzConstants.Wizz);
            ((FizzBuzzModel)actual.Model).FizzBuzzList.ElementAt(4).Should().Be(FizzBuzzConstants.Wuzz);
            ((FizzBuzzModel)actual.Model).FizzBuzzList.ElementAt(14).Should().Be(FizzBuzzConstants.WizzWuzz);
        }
        [Test]
        public void ResultPageReturnsValidList()
        {
            modelInstance = new FizzBuzzModel { Input = 30, EnteredValue = 30 };
            this.mockbunisessLayer.Setup(m => m.GenerateFizzBuzzSequence(It.IsAny<int>())).Returns(
                new List<string>
                {
                    "1",
                    "2",
                    "fizz",
                    "4",
                    "buzz",
                    "fizz",
                    "7",
                    "8",
                    "fizz",
                    "buzz",
                    "11",
                    "fizz",
                    "13",
                    "14",
                    "fizz buzz","16","17","fizz","19","buzz","fizz","22","23","fizz","buzz","26","27","28","29","fizz buzz"

                });

            ViewResult actual = this.fizzbuzzcontroller.ResultPage(this.modelInstance.EnteredValue, 2) as ViewResult;
            actual.Model.As<FizzBuzzModel>().FizzBuzzList.Count().Should().Be(10);

            actual.Model.As<FizzBuzzModel>().FizzBuzzList.ElementAt(0).Should().Be("fizz");

            actual.Model.As<FizzBuzzModel>().FizzBuzzList.ElementAt(2).Should().Be("23");

            actual.Model.As<FizzBuzzModel>().FizzBuzzList.ElementAt(4).Should().Be("buzz");

            actual.Model.As<FizzBuzzModel>().FizzBuzzList.ElementAt(9).Should().Be("fizz buzz");

        }

        [Test]
        public void ResultPageListForWednesday()
        {
            modelInstance = new FizzBuzzModel { Input = 30, EnteredValue = 30 };
            this.mockbunisessLayer.Setup(m => m.GenerateFizzBuzzSequence(It.IsAny<int>())).Returns(
                new List<string>
                {
                    "1",
                    "2",
                    "wizz",
                    "4",
                    "wuzz",
                    "wizz",
                    "7",
                    "8",
                    "wizz",
                    "wuzz",
                    "11",
                    "wizz",
                    "13",
                    "14",
                    "wizz wuzz","16","17","wizz","19","wuzz","wizz","22","23","wizz","wuzz","26","27","28","29","wizz wuzz"
                });
            ViewResult actual = this.fizzbuzzcontroller.ResultPage(this.modelInstance.EnteredValue, 2) as ViewResult;
            actual.Model.As<FizzBuzzModel>().FizzBuzzList.Count().Should().Be(10);
            actual.Model.As<FizzBuzzModel>().FizzBuzzList.ElementAt(0).Should().Be("wizz");

            actual.Model.As<FizzBuzzModel>().FizzBuzzList.ElementAt(2).Should().Be("23");

            actual.Model.As<FizzBuzzModel>().FizzBuzzList.ElementAt(4).Should().Be("wuzz");

            actual.Model.As<FizzBuzzModel>().FizzBuzzList.ElementAt(9).Should().Be("wizz wuzz");

        }

        [TearDown]
        public void Dispose()
        {
            this.modelInstance = null;
            this.fizzbuzzcontroller = null;
            this.mockbunisessLayer = null;

        }
    }
}
